pipeline {
    agent { node { label 'temp-ansible-jenkins-agent' } }
    
    parameters {
        choice(name: 'Invoke_Parameters', choices:"No\nYes", description: "Do you whish to do a dry run to grab parameters?" )
        choice(name: 'openshift_version', choices: ['4.4','4.5','4.6','4.7'], description: 'Openshift Version')
        choice(name: 'operator_type', choices: ['community-operators','certified-operators','redhat-operators','upstream-community-operators'], description: 'Openshift Type')
    }

    environment {
        OPERATOR_REPO='git@github.com:operator-framework/community-operators.git'
        MY_OPERATOR_REPO='git@gitlab.com:rocrisp/operator.git'
    }

    options {
      timestamps()
      timeout(time: 1500, unit: 'MINUTES')
    }
    stages {
        stage ('Checkout toolbox') {
            steps {
                echo 'Get Repo...'
                checkout([
                       $class: 'GitSCM', 
                       branches: [[name: '*/master']], 
                       doGenerateSubmoduleConfigurations: false, 
                       extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'toolbox']], 
                       submoduleCfg: [], 
                       userRemoteConfigs: [[credentialsId: 'gitlabsshkey', url: 'git@gitlab.com:rocrisp/operatorenablementtools.git']]
                ])
            }
        }
        stage('Test Operators') {
            
            steps {
                script {
                    dir("toolbox") {
                        sh """
                        echo 'Download Operator'
                        ./getoperators.exp ${env.WORKSPACE}
                        """
                    }
                }
                script {
                    if ("${params.Invoke_Parameters}" == "Yes") {
                        currentBuild.result = 'ABORTED'
                        error('DRY RUN COMPLETED. JOB PARAMETERIZED.')
                    }
                }
                echo 'Get Repo..'
                script {
                    dir("toolbox/content/${params.operator_type}") {
                        def files = findFiles() 
                        files.each{ f -> 
                            if(f.directory) {
                                echo "Triggering job for ${operator_type} :  ${f.name}"

                                b=build(job: 'testing-cvp-nonproduction', parameters: [
                                    string(name: 'Operator', value: "${f.name}"),
                                    string(name: 'openshift_version', value: "${params.openshift_version}"),
                                    string(name: 'operator_type', value: "${params.operator_type}")
                                    ],propagate: false).result
                                if(b == 'FAILURE') {
                                    echo "${f.name} job failed"
                                    currentBuild.result = 'UNSTABLE' // of FAILURE
                                }
                            }
                        }
                    }  
                }
            }
        }       
    }
}