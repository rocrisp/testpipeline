pipeline {
    
    agent { node { label 'temp-ansible-jenkins-agent' } }
    
    parameters {
        choice(name: 'Invoke_Parameters', choices:"No\nYes", description: "Do you whish to do a dry run to grab parameters?" )
        choice(name: 'openshift_version', choices: ['4.7','4.7mycluster'], description: 'Openshift Version')
        choice(name: 'installoperand', choices: ['yes','no'], description: 'Install Operand')
        choice(name: 'option', choices: ['Retry last good source of truth','none'], description: 'Extra options')


        //choice(name: 'operator_type', choices: ['redhat-operators','certified-operators','community-operators','upstream-community-operators'], description: 'Openshift Type')
    }

    options {
      timestamps()
      timeout(time: 1500, unit: 'MINUTES')
      ansiColor("xterm") 
    }

    environment {
           OFFLINE_CATALOGER='git@github.com:kevinrizza/offline-cataloger.git'
           RUN_TOOLS='git@github.com:rocrisp/install-operator-tools.git'
           KUBECONFIG="${WORKSPACE}/kubeconfig"
    }
    stages {

        stage ('Dry run') {
            steps {
                script {
                    if ("${params.Invoke_Parameters}" == "Yes") {
                        currentBuild.result = 'ABORTED'
                        error('DRY RUN COMPLETED. JOB PARAMETERIZED.')
                    }
                }  
            }  
        }

        stage('Login to Openshift cluster') {
            steps {
                
                script {
                    if ("${params.openshift_version}" == "4.7"){
                        cluster = "ci-testcluster-4.7"
                    } else if ( "${params.openshift_version}" == "4.7mycluster") {
                        cluster = "ci-4.7cluster"
                    }
                    echo "$cluster"

                    withCredentials([file(credentialsId: "$cluster", variable: 'MYFILE')]) {
                        sh '''
                            #!/bin/bash
                            cp ${MYFILE} newsecretfile.txt
                            cp -a newsecretfile.txt kubeconfig
                            chmod 777 kubeconfig
                        '''
                    }
                    sh '''
                    export LANG=en_US.UTF-8
                    oc login -u system:admin -n default
                    oc cluster-info
                    oc whoami
                    '''
                }
            }
        }

        stage ('Install Certified Operators') {
            
            steps {
                echo 'Download repo'
                checkout([
                        $class: 'GitSCM', 
                        branches: [[name: '*/main']], 
                        doGenerateSubmoduleConfigurations: false, 
                        submoduleCfg: [], 
                        userRemoteConfigs: [[credentialsId: 'githubsshkey', url: "${env.RUN_TOOLS}"]]
                ])
                echo 'Generates manifests from appregistry namespace'
                sh """
                    #generate certified operator manifest
                    ./offline-cataloger generate-manifests "certified-operators"
                    
                """
                script {
                    echo 'setup extra options'
                    if ("${params.options}" == "Retry last good source of truth"){
                        sh """
                        export NEWTEST=YES
                        """
                    } 
                    if ("${params.installoperand}" == "yes"){
                        echo 'Install Operator and Operand'
                        sh """
                        echo $NEWTEST
                        #get the manifest directory
                        manifest_directory=\$(find . -name manifest*)
                        ./run.sh \$manifest_directory
                        """
                    } else if ("${params.installoperand}" == "no"){
                        echo 'Install Operator'
                        sh """
                        echo $NEWTEST
                        #get the manifest directory
                        manifest_directory=\$(find . -name manifest*)
                        ./run.sh \$manifest_directory -nooperand
                        """
                    }

                }
                
            }
        }
    }

    post {
        always {
            script {
                echo 'Output results'
                echo '------------------------------------'
                echo 'Output success_operator.txt'
                sh """
                    cat success_operator.txt
                """
                echo 'Output sucess_operand.txt'
                sh """
                   cat success_operand.txt
                """
                echo 'Output failed_operator.txt'
                sh """
                   cat failed_operator.txt
                """
                echo 'Output failed_operand.txt'
                sh """
                   cat failed_operand.txt
                """
            }
        }
        success {
            echo "Job ran successfully."
        }
        failure {
            echo "Job has failed to run successfully."
        }
    }
}
