# install jenkins
oc new-project ci
oc new-app -e OPENSHIFT_ENABLE_OAUTH=true -e VOLUME_CAPACITY=10Gi jenkins-persistent

# Setup Jenkins Permission

Prereq:
Go to manage jenkins, then configure global security. Allow anonymous user to have the following permissions:
Overall: Read
Job: Create
# Add Jenkins Plugins

1. Update all plugins
2. Install the following plugins
    timestamper
    Openshift Pipeline
    pipeline: aws steps
    Gitlab Authentication
    ansicolor
    ansible

# Setup Jenkins credentials

1. /* this job is done by another team now*/
   Add secret key to openshift Jenkins cluster so Jenkins can retrieve later.
   *** provided you have a test cluster
   oc create secret generic testcluster1 --from-file=filename=/Users/rosecrisp/openshift/clusters/testcluster1/auth/kubeconfig  
   oc label secret testcluster1 credential.sync.jenkins.openshift.io=true

2. Add gitlabsshkey
   kind: ssh key

3. Add quaytoken
   kind:secret text

4. Add awscredentials
   kind: AWS Credentials


# Setup Agent to run the jobs
https://gitlab.com/rocrisp/ansible-jenkins-agent

Create a Docker image to run as the agent:
In the cluster do the following:
oc new-build https://gitlab.com/rocrisp/ansible-jenkins-agent.git
oc label is ansible-jenkins-agent role=jenkins-slave
oc describe is/ansible-jenkins-agent

** Don't need to do this step unless you want to rebuild agent
To remove agent:
oc tag -d ansible-jenkins-agent:latest
oc delete bc ansible-jenkins-agent

# Create Jenkins jobs:
From the command line do the following:

Export jenkins to env
export URL='https://jenkins-tempjenkinsproject.apps.cvptestingcluster1.coreostrain.me'
cd to testpipeline where setup-agent.xml is located on your localdrive
Create job
curl --insecure -s -XPOST "$URL/createItem?name=setup-agent" --data-binary @setup-agent.xml -H "Content-Type:text/xml" -v
curl --insecure -s -XPOST "$URL/createItem?name=setup-aws" --data-binary @setup-aws.xml -H "Content-Type:text/xml" -v
curl --insecure -s -XPOST "$URL/createItem?name=setup-gitlab" --data-binary @setup-gitlab.xml -H "Content-Type:text/xml" -v
curl --insecure -s -XPOST "$URL/createItem?name=setup-oc" --data-binary @setup-oc.xml -H "Content-Type:text/xml" -v
curl --insecure -s -XPOST "$URL/createItem?name=cvptest" --data-binary @cvptest.xml -H "Content-Type:text/xml" -v
curl --insecure -s -XPOST "$URL/createItem?name=kitchensink-sequence" --data-binary @kitchensink-sequence.xml -H "Content-Type:text/xml" -v
curl --insecure -s -XPOST "$URL/createItem?name=cvptest-list" --data-binary @cvptest-list.xml -H "Content-Type:text/xml" -v


Build job
curl --insecure -s -XPOST "$URL/job/NEWJOB/build" --data-binary @pipeline.xml -H "Content-Type:text/xml" -v

Read a job 
curl --insecure -X GET https://jenkins-tempjenkinsproject.apps.cvptestingcluster1.coreostrain.me/job/test-pipeline/config.xml

Install plugin *** Not working***
curl --insecure -s -X POST -d '<jenkins><install plugin="timestamper@version" /></jenkins>' --header 'Content-Type: text/xml' $URL/pluginManager/installNecessaryPlugins

# setup mysql
go to your local repo https://gitlab.com/rocrisp/testresultviewer.git

Run
oc new-app openshift/templates/cakephp-mysql.json -p SOURCE_REPOSITORY_URL=https://gitlab.com/rocrisp/testresultviewer.git

Update the pod's name in create_database.sh

run ./create_database.sh

