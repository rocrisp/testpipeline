pipeline {
    
    agent { node { label 'temp-ansible-jenkins-agent' } }
    
    parameters {
        choice(name: 'Invoke_Parameters', choices:"No\nYes", description: "Do you whish to do a dry run to grab parameters?" )
        string(defaultValue: "3scale-operator", description: 'Operator Name', name: 'Operator')
        choice(name: 'openshift_version', choices: ['4.4','4.5','4.6','4.7'], description: 'Openshift Version')
        choice(name: 'operator_type', choices: ['redhat-operators','certified-operators','community-operators','upstream-community-operators'], description: 'Openshift Type')
    }

    options {
      timestamps()
      timeout(time: 120, unit: 'MINUTES')
      ansiColor("xterm") 
    }

    environment {
        OPERATOR_REPO='git@github.com:operator-framework/community-operators.git'
        MY_OPERATOR_REPO='git@gitlab.com:rocrisp/operator.git'
        CVP_REPO='git@github.com:redhat-operator-ecosystem/operator-test-playbooks.git'
        //CVP_REPO='git@github.com:rocrisp/operator-test-playbooks.git'
        QUAY_NAMESPACE='rocrisp'
        OPERATOR_DIR="${WORKSPACE}/operator"
        PLAYBOOK_DIR="${WORKSPACE}/operator-test-playbooks"
        KUBECONFIG="${WORKSPACE}/kubeconfig"
        ANSIBLE_ENABLE_TASK_DEBUGGER="True"
        ANSIBLE_FORCE_COLOR=true
        LANG='en_US.UTF-8'
        S3FB="https://s3.console.aws.amazon.com/s3/buckets/jenkinsoperatorenablement/cvp-test-results/errors/${params.operator_type}/${params.Operator}/?region=us-east-2&tab=overview"
        S3SB="https://s3.console.aws.amazon.com/s3/object/jenkinsoperatorenablement/cvp-test-results/${params.operator_type}/${params.Operator}_success.txt?region=us-east-2&tab=overview"
    }

    stages {

        stage ('Initializing tools') {

            steps {
                script {
                    if ("${params.Invoke_Parameters}" == "Yes") {
                        currentBuild.result = 'ABORTED'
                        error('DRY RUN COMPLETED. JOB PARAMETERIZED.')
                    }
                }  
                echo 'Verify tools exist.'

                sh 'operator-courier --version'
                sh 'ansible-playbook --version'
            }
        }

        stage ('Checkout CVP') {
            
            steps {
                echo 'Get Repo...'
                checkout([
                       $class: 'GitSCM', 
                       branches: [[name: '*/master']], 
                       doGenerateSubmoduleConfigurations: false, 
                       submoduleCfg: [], 
                       userRemoteConfigs: [[credentialsId: 'githubsshkey', url: "${env.CVP_REPO}"]]
                    ])
            }
        }

        stage ('Checkout toolbox') {
            
            steps {
                echo 'Get Repo...'
                checkout([
                       $class: 'GitSCM', 
                       branches: [[name: '*/master']], 
                       doGenerateSubmoduleConfigurations: false, 
                       extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'toolbox']], 
                       submoduleCfg: [], 
                       userRemoteConfigs: [[credentialsId: 'gitlabsshkey', url: 'git@gitlab.com:rocrisp/operatorenablementtools.git']]
                ])
            }
        }

        stage('Download Operators') {

            steps {
                script {
                    dir("toolbox") {
                        sh """
                        echo 'Download Operator'
                        ./getoperators.exp ${env.WORKSPACE}
                        """
                    }
                }
                script {
                    sh """
                        if [ -f toolbox/content/${params.operator_type}/${params.Operator}/*.yaml ]; then 
                            cp -a toolbox/content/${params.operator_type}/${params.Operator} ${OPERATOR_DIR} 
                        elif [ -f toolbox/content/${params.operator_type}/${params.Operator}/*/*.yaml ]; then 
                            cp -a toolbox/content/${params.operator_type}/${params.Operator}/${params.Operator}* ${OPERATOR_DIR}
                        fi

                        ls 
                        
                        if [ -f ${OPERATOR_DIR}/bundle.yaml ]; then
                           ./toolbox/bin/convert_bundle_pkg.sh ${OPERATOR_DIR} bundle.yaml ${params.Operator}
                        else 
                           echo "The metadata is already converted to packages.yaml"
                        fi
                    """
                }
            }
        }

        stage('Run CVP for community operators') {
            when {
            expression {
                return params.operator_type == 'community-operators';
                }
            }
            steps {
                echo 'Run this stage - when is operator type is community-operators'
                
                script {

                    if ("${params.openshift_version}" == "4.4"){
                        cluster = "testcluster-4.4"
                    } else if ( "${params.openshift_version}" == "4.5") {
                        cluster = "testcluster-4.5"
                    } else if ( "${params.openshift_version}" == "4.6") {
                        cluster = "testcluster-4.6"
                    } else if ( "${params.openshift_version}" == "4.7") {
                        cluster = "testcluster-4.7"
                    }
                    echo "$cluster"
                }
                
                withCredentials([file(credentialsId: "ci-$cluster", variable: 'MYFILE')]) {
                    sh 'echo "Get cluster kubeconfig file"'
                    sh '''
                    #!/bin/bash
                    cp ${MYFILE} newsecretfile.txt
                    cp -a newsecretfile.txt kubeconfig
                    chmod 777 kubeconfig
                    '''
                    withCredentials([string(credentialsId: 'quaytoken', variable: 'TOKEN')]) {
                        ansiColor('xterm') {
                            sh 'oc login -u system:admin -n test'
                            sh """
                               set +e
                               oc delete secret marketplacesecret -n openshift-marketplace
                               oc delete project test-operator-${params.Operator}
                               set -e
                            """
                            sh 'pwd;ls'
                            ansiblePlaybook(
                                playbook: "local-test-operator-bundle.yml",
                                extras: """-vv -i "localhost," --connection=local \
                                         -e quay_token="${TOKEN}" \
                                         -e kubeconfig_path="${KUBECONFIG}" \
                                         -e quay_namespace="${QUAY_NAMESPACE}" \
                                         -e production_quay_namespace="community-operators" \
                                         -e openshift_namespace="test-operator-${params.Operator}" \
                                         -e operator_dir="${OPERATOR_DIR}" \
                                         -e run_imagesource=false \
                                         -e run_deploy=true""",
                                colorized: true
                            )
                        }
                    }
                }
            }
        }

        stage('Run CVP for ISV (Certified) operators') {
            when {
            expression {
                return params.operator_type == 'certified-operators';
                }
            }
            steps {
                echo 'Run this stage - when is operator type is certified-operators'
                
                script {

                    if ("${params.openshift_version}" == "4.4"){
                        cluster = "testcluster-4.4"
                    } else if ( "${params.openshift_version}" == "4.5") {
                        cluster = "testcluster-4.5"
                    } else if ( "${params.openshift_version}" == "4.6") {
                        cluster = "testcluster-4.6"
                    } else if ( "${params.openshift_version}" == "4.7") {
                        cluster = "testcluster-4.7"
                    }
                    echo "$cluster"
                }
                
                withCredentials([file(credentialsId: "ci-$cluster", variable: 'MYFILE')]) {
                    sh 'echo "Get cluster kubeconfig file"'
                    sh '''
                    #!/bin/bash
                    cp ${MYFILE} newsecretfile.txt
                    cp -a newsecretfile.txt kubeconfig
                    chmod 777 kubeconfig
                    '''
                    withCredentials([string(credentialsId: 'quaytoken', variable: 'TOKEN')]) {
                        ansiColor('xterm') {
                            sh 'oc login -u system:admin -n test'
                            sh """
                               set +e
                               oc delete secret marketplacesecret -n openshift-marketplace
                               oc delete project test-operator-${params.Operator}
                               set -e
                            """
                            sh 'pwd;ls'
                            ansiblePlaybook(
                                playbook: "local-test-operator-bundle.yml",
                                extras: """-vv -i "localhost," --connection=local \
                                         -e quay_token="${TOKEN}" \
                                         -e kubeconfig_path="${KUBECONFIG}" \
                                         -e quay_namespace="${QUAY_NAMESPACE}" \
                                         -e production_quay_namespace="certified-operators" \
                                         -e operator_dir="${OPERATOR_DIR}""",
                                colorized: true
                            )
                        }
                    }
                }
            }
        }

        stage('Run CVP for Red Hat Operators') {
            when {
            expression {
                return params.operator_type == 'redhat-operators';
                }
            }
            steps {
                script {

                    if ("${params.openshift_version}" == "4.4"){
                        cluster = "testcluster-4.4"
                    } else if ( "${params.openshift_version}" == "4.5") {
                        cluster = "testcluster-4.5"
                    } else if ( "${params.openshift_version}" == "4.6") {
                        cluster = "testcluster-4.6"
                    } else if ( "${params.openshift_version}" == "4.7") {
                        cluster = "testcluster-4.7"
                    }
                    echo "$cluster"
                }
                
                withCredentials([file(credentialsId: "ci-$cluster", variable: 'MYFILE')]) {
                    sh 'echo "Get cluster kubeconfig file"'
                    sh '''
                    #!/bin/bash
                    cp ${MYFILE} newsecretfile.txt
                    cp -a newsecretfile.txt kubeconfig
                    chmod 777 kubeconfig
                    '''
                    withCredentials([string(credentialsId: 'quaytoken', variable: 'TOKEN')]) {
                        ansiColor('xterm') {
                            // sh 'oc login -u system:admin -n test'
                            // sh """
                            //    set +e
                            //    oc delete secret marketplacesecret -n openshift-marketplace
                            //    oc delete project test-operator-${params.Operator}
                            //    set -e
                            // """
                            sh 'pwd;ls'
                            ansiblePlaybook(
                                playbook: "local-test-operator.yml",
                                extras: """-vv -i "localhost," --connection=local \
                                         -e production_quay_namespace="certified-operators" \
                                         -e operator_dir="${OPERATOR_DIR}" \
                                         -e openshift_namespace="test-operator-${params.Operator}" \
                                         -e run_deploy=false""",
                                colorized: true
                            )
                        }
                    }
                }
            }
        }

        stage('Output Results') {

            steps {
                echo 'Output from scorecard....'
                sh """
                   cd /tmp/operator-test
                   cat *scorecard-results.json \
                        | jq ".results[] | {name: .name, state: .state, desc: .description}" > ${env.WORKSPACE}/${params.Operator}_success.txt
                   cat ${env.WORKSPACE}/${params.Operator}_success.txt     
                """
            }    
        }
    }

    post {
        always {
            script {
                if ("${params.Invoke_Parameters}" == "No") {
                    withAWS(region:'us-east-2',credentials:'awscredentials') {
                        s3Delete bucket: 'jenkinsoperatorenablement', 
                        path:"cvp-test-results/${params.operator_type}/${params.Operator}_failure.txt"
                        s3Delete bucket: 'jenkinsoperatorenablement', 
                        path:"cvp-test-results/${params.operator_type}/${params.Operator}_success.txt"
                    }        
                }   
            }
        }
        success {
            sh "ls -ltr /tmp/operator-test/*; cp -r /tmp/operator-test ${env.WORKSPACE};cat ${env.WORKSPACE}/${params.Operator}_success.txt"
            sh "python3 ./toolbox/insert_status.py ${params.Operator} ${params.operator_type} ${params.openshift_version} success ${env.S3SB}"
        }
        failure {
            script {
                sh "python3 ./toolbox/insert_status.py ${params.Operator} ${params.operator_type} ${params.openshift_version} failed ${env.S3FB}"
                sh """
                   cd /tmp/operator-test
                   ls -ltr
                   cp *.txt ${env.WORKSPACE}/.
                   echo ${params.Operator} build failed > ${env.WORKSPACE}/${params.Operator}_failure.txt
                   cat ${env.WORKSPACE}/${params.Operator}_failure.txt
                """
            }
        }
        cleanup {
            script {
                withAWS(region:'us-east-2',credentials:'awscredentials') {
                    s3Upload bucket: 'jenkinsoperatorenablement', path:"cvp-test-results/${params.operator_type}/", 
                    workingDir:"${env.WORKSPACE}", includePathPattern:"${params.Operator}_*.txt"
                    s3Upload bucket: 'jenkinsoperatorenablement', path:"cvp-test-results/errors/${params.operator_type}/${params.Operator}/", 
                    workingDir:"${env.WORKSPACE}", includePathPattern:"*"

                }
            }
        }
    }
}