
@Library('pipeline-library') _

pipeline {
    agent { node { label 'temp-ansible-jenkins-agent' } }
    
    parameters {
        choice(name: 'Invoke_Parameters', choices:"No\nYes", description: "Do you whish to do a dry run to grab parameters?" )
        file(name: 'bundle', description: 'Choose a zip file to upload')
        string(defaultValue: "Cortex-operator", description: 'Operator', name: 'Operator')
        choice(name: 'openshift_version', choices: ['4.4','4.5','4.6'], description: 'Openshift Version')
        choice(name: 'operator-sdk-version', choices: ['1.0.0','1.0.1'], description: 'Operator Sdk version')
    }

    options {
      timestamps()
      timeout(time: 120, unit: 'MINUTES')
      ansiColor('xterm')
    }

    environment {
        OPERATOR_REPO='git@github.com:operator-framework/community-operators.git'
        MY_OPERATOR_REPO='git@gitlab.com:rocrisp/operator.git'
        //CVP_REPO='git@github.com:redhat-operator-ecosystem/operator-test-playbooks.git'
        CVP_REPO='git@gitlab.com:rocrisp/cvp-playbook.git'
        QUAY_NAMESPACE='rocrisp'
        OPERATOR_DIR="${WORKSPACE}/operator"
        PLAYBOOK_DIR="${WORKSPACE}/operator-test-playbooks"
        KUBECONFIG="${WORKSPACE}/kubeconfig"
        ANSIBLE_ENABLE_TASK_DEBUGGER="True"
        ANSIBLE_FORCE_COLOR=true
        LANG='en_US.UTF-8'
        S3FB="https://s3.console.aws.amazon.com/s3/buckets/jenkinsoperatorenablement/cvp-test-results/errors/${params.operator_type}/${params.Operator}/?region=us-east-2&tab=overview"
        S3SB="https://s3.console.aws.amazon.com/s3/object/jenkinsoperatorenablement/cvp-test-results/${params.operator_type}/${params.Operator}_success.txt?region=us-east-2&tab=overview"
    }

    stages {

        stage ('Initializing tools') {

            steps {

                script {
                    if ("${params.Invoke_Parameters}" == "Yes") {
                        currentBuild.result = 'ABORTED'
                        error('DRY RUN COMPLETED. JOB PARAMETERIZED.')
                    }
                }  
                echo 'Verify tools exist.'
                sh 'operator-courier --version'
                sh 'ansible-playbook --version'
            }
        }

        stage ('Checkout CVP') {
            
            steps {
                echo 'Get Repo...'
                checkout([
                       $class: 'GitSCM', 
                       branches: [[name: '*/master']], 
                       doGenerateSubmoduleConfigurations: false, 
                       submoduleCfg: [], 
                       userRemoteConfigs: [[credentialsId: 'gitlabsshkey', url: "${env.CVP_REPO}"]]
                    ])
            }
        }

        stage('Download Operators') {

            steps {
                script {
                    def file_in_workspace = unstashParam "bundle"
                    sh 'ls -ltr'
                    dir("operator") {
                       sh """
                           echo 'Unzip Operator bundle ${file_in_workspace}'
                           unzip ../${file_in_workspace}
                            ls -ltr
                            """
                    }
                }
            }
        }

        stage("Run CVP") {
            steps {
                
                script {
                    if ("${params.openshift_version}" == "4.4"){
                        cluster = "testcluster-4.4"
                    } else if ( "${params.openshift_version}" == "4.5") {
                        cluster = "testcluster-4.5"
                    } else if ( "${params.openshift_version}" == "4.6") {
                        cluster = "testcluster-4.6"
                    }
                    echo "$cluster"
                }
                
                withCredentials([file(credentialsId: "ci-$cluster", variable: 'MYFILE')]) {
                    sh 'echo "Get cluster kubeconfig file"'
                    sh '''
                    #!/bin/bash
                    cp ${MYFILE} newsecretfile.txt
                    cp -a newsecretfile.txt kubeconfig
                    chmod 777 kubeconfig
                    '''
                    withCredentials([string(credentialsId: 'quaytoken', variable: 'TOKEN')]) {
                        ansiColor('xterm') {
                            sh 'oc login -u system:admin -n test'
                            
                            sh """
                               set +e
                               oc delete secret marketplacesecret -n openshift-marketplace
                               oc delete project test-operator
                               set -e
                            """

                            ansiblePlaybook(
                                playbook: "local-test-operator.yml",
                                extras: """-vv -i "localhost," --connection=local \
                                         -e quay_token="${TOKEN}" \
                                         -e kubeconfig_path="${KUBECONFIG}" \
                                         -e quay_namespace="${QUAY_NAMESPACE}" \
                                         -e production_quay_namespace="community-operators" \
                                         -e openshift_namespace="test-operator" \
                                         -e operator_dir="${OPERATOR_DIR}" \
                                         -e run_imagesource=false \
                                         -e run_catalog_init=false """,
                                colorized: true
                            )
                        }
                    }
                }
            }
        }

        stage('Output Results') {

            steps {
                echo 'Output from scorecard....'
                sh """
                   cd /tmp/operator-test
                   cat *scorecard-results.json \
                        | jq ".results[] | {name: .name, state: .state, desc: .description}" > ${env.WORKSPACE}/${params.Operator}_success.txt
                   cat ${env.WORKSPACE}/${params.Operator}_success.txt     
                """
            }    
        }
    }

    post {
        always {
            script {
                if ("${params.Invoke_Parameters}" == "No") {
                    withAWS(region:'us-east-2',credentials:'awscredentials') {
                        s3Delete bucket: 'jenkinsoperatorenablement', 
                        path:"cvp-test-results/${params.operator_type}/${params.Operator}_failure.txt"
                        s3Delete bucket: 'jenkinsoperatorenablement', 
                        path:"cvp-test-results/${params.operator_type}/${params.Operator}_success.txt"
                    }        
                }   
            }
        }
        success {
            sh "ls -ltr /tmp/operator-test/*; cp -r /tmp/operator-test ${env.WORKSPACE};cat ${env.WORKSPACE}/${params.Operator}_success.txt"
            sh "python3 ./toolbox/insert_status.py ${params.Operator} ${params.operator_type} ${params.openshift_version} success ${env.S3SB}"
        }
        failure {
            script {
                sh "python3 ./toolbox/insert_status.py ${params.Operator} ${params.operator_type} ${params.openshift_version} failed ${env.S3FB}"
                sh """
                   cd /tmp/operator-test
                   ls -ltr
                   cp *.txt ${env.WORKSPACE}/.
                   echo ${params.Operator} build failed > ${env.WORKSPACE}/${params.Operator}_failure.txt
                   cat ${env.WORKSPACE}/${params.Operator}_failure.txt
                """
            }
        }
        cleanup {
            script {
                withAWS(region:'us-east-2',credentials:'awscredentials') {
                    s3Upload bucket: 'jenkinsoperatorenablement', path:"cvp-test-results/${params.operator_type}/", 
                    workingDir:"${env.WORKSPACE}", includePathPattern:"${params.Operator}_*.txt"
                    s3Upload bucket: 'jenkinsoperatorenablement', path:"cvp-test-results/errors/${params.operator_type}/${params.Operator}/", 
                    workingDir:"${env.WORKSPACE}", includePathPattern:"*"

                }
            }
        }
    }
}